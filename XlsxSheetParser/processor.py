""" Script to read and display the students information. """
# pylint: disable = invalid-name, too-few-public-methods

import argparse
import xlrd

class StudentXlsxSheetParser():
    """  Class for parsing the xlsx data. """
    students = []

    def __init__(self, xlsxFilename, sheetName):
        self.xlsxFilename = xlsxFilename
        self.sheetName = sheetName

    def parse(self):
        """ Function to parse the student data from xlsx sheet. """
        print("Parsing %s sheet of %s XLS" %(self.sheetName, self.xlsxFilename))
        studentInfoXlsxFilePath = self.xlsxFilename
        workBookObj = xlrd.open_workbook(studentInfoXlsxFilePath)
        workSheetObj = workBookObj.sheet_by_name(self.sheetName)
        total_rows = workSheetObj.nrows

        for current_row in range(total_rows):
            row_data_values_list = workSheetObj.row_values(current_row)
            self.students.append(StudentProcessor(row_data_values_list))

class StudentProcessor():
    """ class to show the details of students. """

    def __init__(self, row_data_values_list):
        self.studentId = row_data_values_list[0]
        self.studentName = row_data_values_list[1]
        self.studentAddress = row_data_values_list[2]
        self.studentSubjects = row_data_values_list[3]

    def display_data(self):
        """ Function to display the student data. """
        print("----------")
        print("Student ID:", self.studentId)
        print("Student Name:", self.studentName)
        print("Student Address:", self.studentAddress)
        print("Student Subjects:", self.studentSubjects)
        print("----------")

def processor(xlsxFilename, sheetName):
    """ Key function to get xlsx sheet data and to create instances of every student. """

    studentXlsxSheetParser = StudentXlsxSheetParser(xlsxFilename, sheetName)
    studentXlsxSheetParser.parse()
    print("Displaying the students data...")
    for studentInfo in studentXlsxSheetParser.students:
        studentInfo.display_data()

# To launch the file as a script.
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("xlsxFilename", help="XLSX file name")
    parser.add_argument("sheetName", help="Sheet to be parsed")
    args = parser.parse_args()
    processor(args.xlsxFilename, args.sheetName)
