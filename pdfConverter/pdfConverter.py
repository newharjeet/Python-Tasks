# Import all the required modules 
import os
import win32com.client

# FileFormat = 17 is the value for a .pdf file
wdFormatPDF = 17

# Variables initialization
CURR_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
input_file = os.path.join(CURR_DIR_PATH, "Py_tools.docx")
output_file = os.path.join(CURR_DIR_PATH, "myfile.pdf")

def DocToPdfConverter():
    """ Function to convert doc files into pdf files. """
    try:
        word = win32com.client.Dispatch("Word.Application") # create COM object
        doc = word.Documents.Open(input_file) # open docx file
        doc.SaveAs(output_file, FileFormat=wdFormatPDF) # conversion
        doc.Close() # close docx file
        word.Quit() # close Word Application
        print("[SUCCESS]: File converted successfully into PDF file.")
    except Exception as error:
        print("[ERROR]:", error)

# To launch the file as a script.
if __name__ == "__main__":
    DocToPdfConverter()
    
