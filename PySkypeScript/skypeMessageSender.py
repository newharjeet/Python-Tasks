from skpy import Skype,SkypeContactGroup
import argparse
import getpass

def msgSender(username, password):

    try:
        sk = Skype(username, password)
        print("[SUCCESS]: Connection Established.")
        print(sk.user)
    except:
        print("[ERROR]: Couldn't create connection.")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("username", help="User Name")
    args = parser.parse_args()
    password = getpass.getpass("Your skype password:")

    msgSender(args.username, password)
