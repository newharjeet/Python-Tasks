import argparse

def dataReader(dataFileName):

    with open(dataFileName) as dataFileObj:
        for line in dataFileObj:
            if(len(line.strip()) == 0):
                pass
            else:
                variablesList = line.split(" ")
                pattern = "Name:" + variablesList[0] + " Email:" + variablesList[1] + " Mob:" + variablesList[2]
                print(pattern)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("dataFileName", help="Data File Name")
    args = parser.parse_args()
    dataReader(args.dataFileName)
    
